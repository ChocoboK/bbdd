DROP DATABASE IF EXISTS ejercicio1t3t4;
CREATE DATABASE ejercicio1t3t4;
USE ejercicio1t3t4;

CREATE TABLE CLIENTE(DNI CHAR(9), nombre VARCHAR(20), apellidos VARCHAR(40), telefono CHAR(9), email VARCHAR(40) UNIQUE, CONSTRAINT PK_CLIENTES PRIMARY KEY (DNI));

CREATE TABLE TIENDA(nombre VARCHAR(30), provincia VARCHAR(40), localidad VARCHAR(40), direccion VARCHAR(60), telefono CHAR(9), diaApertura ENUM('Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo'), diaCierre ENUM('Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo'), horaApertura TINYINT UNSIGNED, horaCierra TINYINT UNSIGNED, CONSTRAINT PK_TIENDA PRIMARY KEY (nombre));

CREATE TABLE OPERADORA(nombre VARCHAR(20), colorlogo ENUM('Rojo','Azul','Naranja','Morado'), porcentajecobertura TINYINT UNSIGNED COMMENT '%', frecuenciaGSM SMALLINT UNSIGNED, paginaweb VARCHAR(100), CONSTRAINT PK_OPERADORA PRIMARY KEY(nombre));

CREATE TABLE TARIFA(nombre VARCHAR(50), nombre_operadora VARCHAR(20), tamanodatos TINYINT UNSIGNED, tipodatos ENUM('MB','GB','TB'), minutosgratis SMALLINT UNSIGNED, SMSgratis ENUM('Si','No'), CONSTRAINT PK_TARIFA PRIMARY KEY(nombre),
    CONSTRAINT FK_TARIFA_OPERADORA FOREIGN KEY (nombre_operadora) REFERENCES OPERADORA(nombre));

CREATE TABLE MOVIL(marca VARCHAR(20), modelo VARCHAR(30), descripcion VARCHAR(300), SO ENUM('iOS','Android'), RAM TINYINT UNSIGNED, pulgadaspantalla DECIMAL(3,2), camarampx DECIMAL(2,2), CONSTRAINT PK_MOVIL PRIMARY KEY(marca,modelo));

CREATE TABLE MOVIL_LIBRE(marca_movil VARCHAR(20), modelo_movil VARCHAR(20), precio VARCHAR(20),
    CONSTRAINT FK_LIBRE_MOVIL FOREIGN KEY (marca_movil, modelo_movil) REFERENCES MOVIL(marca, modelo));

CREATE TABLE MOVIL_CONTRATO(marca_movil VARCHAR(20), modelo_movil VARCHAR(30), nombre_operadora VARCHAR(20), precio DECIMAL(6,2),
    CONSTRAINT FK_CONTRATO_MOVIL FOREIGN KEY(marca_movil, modelo_movil) REFERENCES MOVIL(marca,modelo),
    CONSTRAINT FK_CONTRATO_OPERADORA FOREIGN KEY(nombre_operadora) REFERENCES OPERADORA(nombre));

CREATE TABLE OFERTA(nombre_operadora_tarifa VARCHAR(20), nombre_tarifa VARCHAR(50), marca_movil_contrato VARCHAR(20), modelo_movil_contrato VARCHAR (30),
    CONSTRAINT FK_OFERTA_TARIFA FOREIGN KEY (nombre_operadora_tarifa, nombre_tarifa) REFERENCES TARIFA(nombre_operadora, nombre),
    CONSTRAINT FK_OFERTA_MOVIL_CONTRATO FOREIGN KEY(marca_movil_contrato, modelo_movil_contrato) REFERENCES MOVIL_CONTRATO(marca_movil, modelo_movil));

CREATE TABLE COMPRA(DNI_cliente VARCHAR(20), nombre_tienda VARCHAR(20), marca_movil_libre VARCHAR(20), modelo_movil_libre VARCHAR(20), dia DATE,
    FOREIGN KEY(DNI_cliente) REFERENCES CLIENTE(DNI), FOREIGN KEY(nombre_tienda) REFERENCES TIENDA(nombre),
    FOREIGN KEY(marca_movil_libre, modelo_movil_libre) REFERENCES MOVIL_LIBRE(marca_movil, modelo_movil));

CREATE TABLE CONTRATO(DNI_cliente VARCHAR(20), nombre_tienda VARCHAR(20), nombre_operadora_tarifa_oferta VARCHAR(20), nombre_tarifa_oferta VARCHAR(20), marca_movil_contrato_oferta VARCHAR(20), modelo_movil_contrato_oferta VARCHAR(20), dia DATE,
    FOREIGN KEY(DNI_cliente) REFERENCES CLIENTE(DNI),
    FOREIGN KEY(nombre_tienda) REFERENCES TIENDA(nombre),
    FOREIGN KEY(nombre_operadora_tarifa_oferta, nombre_tarifa_oferta, marca_movil_contrato_oferta, modelo_movil_contrato_oferta)
     REFERENCES OFERTA(nombre_operadora_tarifa, nombre_tarifa, marca_movil_contrato, modelo_movil_contrato));
