DROP DATABASE IF EXISTS Ejercicio1Repaso;
CREATE DATABASE Ejercicio1Repaso;
USE Ejercicio1Repaso;

CREATE TABLE PERSONA(nombre VARCHAR(20), apellidos VARCHAR(40), trabajo VARCHAR(30), PRIMARY KEY(nombre, apellidos));
CREATE TABLE OBJETO(nombre VARCHAR(20), tamaño ENUM('pequeño', 'mediano', 'grande'), nombreOBJETOcontenedor VARCHAR(20), PRIMARY KEY(nombre), FOREIGN KEY (nombreOBJETOcontenedor) REFERENCES OBJETO(nombre));
CREATE TABLE SITUACION(hora TINYINT UNSIGNED, lugar VARCHAR(30), nombrePERSONA VARCHAR(20), apellidosPERSONA VARCHAR(40), vestuario VARCHAR(20), mercancia VARCHAR(20), PRIMARY KEY (hora), FOREIGN KEY (nombrePERSONA, apellidosPERSONA) REFERENCES PERSONA (nombre, apellidos));
CREATE TABLE lleva(horaSITUACION TINYINT UNSIGNED, nombreOBJETO VARCHAR(20), FOREIGN KEY (horaSITUACION) REFERENCES SITUACION(hora), FOREIGN KEY (nombreOBJETO) REFERENCES OBJETO(nombre));

/* DATOS */
INSERT INTO PERSONA VALUES
('Walter', 'White', 'Profesor'),
('Jessie', 'Pinkman', 'Dealer'),
('Saul', 'Goodman', 'Abogado');

INSERT INTO OBJETO VALUES
('Coche', 'grande', NULL),
('Mochila', 'mediano', 'Coche'),
('Pistola', 'pequeño', 'Mochila');

INSERT INTO SITUACION VALUES
(9, 'Caravana', 'Jessie', 'Pinkman', 'Heisenberg', 'Baby Blue'),
(10, 'Despacho', 'Saul', 'Goodman', 'Traje', 'Pasta $$$');

INSERT INTO lleva VALUES
(9, 'Mochila'),
(10, 'Coche');
