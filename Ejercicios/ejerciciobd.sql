/* añadir ADMIN para tres tablas
ADMIN puede quitar usuarios, tambien tiene un gerente (gerente es igual que el Admin, solo que no puede quitar usuarios)
tablas: CLIENTE, PRODUCTO, PEDIDO
CLIENTE tiene Comercial con Modificar / Leer, el cliente deberia poder usar algunos campos por si quiere editar sus datos.
PRODUCTO tiene Cajero
USUARIO puede ir a CLIENTE Y PEDIDO.
*/

DROP DATABASE IF EXISTS practicausuarios;
CREATE DATABASE practicausuarios;
USE practicausuarios;

CREATE TABLE CLIENTE(
        nombre VARCHAR(20),
        apellido VARCHAR(30),
        direccion VARCHAR(50),
        telefono CHAR(9),
        CONSTRAINT PK_C PRIMARY KEY(nombre, apellido));

CREATE TABLE PRODUCTO(
        codigo INT UNSIGNED,
        nombre VARCHAR(30),
        cantidad INT UNSIGNED,
        tamano ENUM('Grande', 'Mediano', 'Pequeño'),
        CONSTRAINT PK_P PRIMARY KEY (codigo));

CREATE TABLE PEDIDO(
        ID INT UNSIGNED AUTO_INCREMENT,
        nombre_cliente VARCHAR(20),
        apellido_cliente VARCHAR(30),
        codigo_producto INT UNSIGNED,
        FechaPedido DATE,
        CONSTRAINT PK_PD PRIMARY KEY (ID),
        CONSTRAINT FK_PEDIDO_CLIENTE FOREIGN KEY(nombre_cliente, apellido_cliente) REFERENCES CLIENTE(nombre, apellido),
        CONSTRAINT FK_PEDIDO_PRODUCTO FOREIGN KEY(codigo_producto) REFERENCES PRODUCTO(codigo));

