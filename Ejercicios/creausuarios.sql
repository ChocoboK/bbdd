/* CREATE USER 'nombre@localhost' IDENTIFIED BY 'contraseña';
GRANT ALL PRIVILEGES ON * (tambien puede ser *.*) TO 'nombre@localhost'; */

USE practicausuarios;

CREATE USER IF NOT EXISTS 'admin'/*@'localhost'*/ IDENTIFIED BY 'admin';
GRANT ALL PRIVILEGES ON practicausuarios.* TO 'admin';

CREATE USER IF NOT EXISTS 'gerente'/*@'localhost'*/ IDENTIFIED BY 'gerente';
GRANT DELETE, INSERT, SELECT, UPDATE ON practicausuarios.* TO 'gerente';

CREATE USER IF NOT EXISTS 'usuario'/*@'localhost'*/ IDENTIFIED BY 'usuario';
GRANT SELECT ON practicausuarios.CLIENTE TO 'usuario';
GRANT SELECT ON practicausuarios.PEDIDO TO 'usuario';
GRANT SELECT ON practicausuarios.PRODUCTO TO 'usuario';

CREATE USER IF NOT EXISTS 'antonio'/*@'localhost'*/ IDENTIFIED BY 'antonio';
GRANT SELECT ON practicausuarios.CLIENTE TO 'antonio';
GRANT SELECT ON practicausuarios.PEDIDO TO 'antonio';
GRANT SELECT ON practicausuarios.PRODUCTO TO 'antonio';

CREATE USER IF NOT EXISTS 'cajero'/*@'localhost'*/ IDENTIFIED BY 'cajero';
GRANT SELECT, UPDATE ON practicausuarios.PRODUCTO TO 'cajero';

CREATE USER IF NOT EXISTS 'comercial'/*@'localhost'*/ IDENTIFIED BY 'comercial';
GRANT SELECT, INSERT, UPDATE ON practicausuarios.PRODUCTO TO 'comercial';
GRANT SELECT, UPDATE ON practicausuarios.CLIENTE 'comercial';
