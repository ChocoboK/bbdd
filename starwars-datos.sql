INSERT INTO ACTOR(nombre, fecha) VALUES
('Ewan McGregor', '1971-03-31'),
('Hayden Christensen', '1981-04-19'),
('Ian McDiarmid', '1944-08-11');

INSERT INTO PERSONAJE(nombre, grado, codigo_ACTOR, codigoSuperior_PERSONAJE) VALUES
('ObiWan KenoVe',7,1,NULL),
('Lucas SkyWalker',9,2,1),
('Emperador Palpame',10,3,NULL);
