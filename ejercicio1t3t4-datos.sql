INSERT INTO CLIENTE VALUES
('12345678A','Perico','de los palotes','123456789','perico@todo.com'),
('11111111Z','Fulanito','de Tal','666666669','ful@nito.com');

INSERT INTO TIENDA VALUES
('Gran via','Madrid','Madrid','C/Gran via 32','141414142','Lunes','Viernes',9,18),
('Castellana','Madrid','Madrid','C/Castellana 108','432123321','Lunes','Sabado',8,15);

INSERT INTO OPERADORA VALUES
('Vodafone','Rojo',99,540,'www.vodafone.com'),
('Movistar','Azul',98,560,'www.movistar.com');

INSERT INTO TARIFA VALUES
('Koala','Vodafone',4,'GB',200,'Si'),
('Ardilla', 'Vodafone',3,'GB',300,'Si');
